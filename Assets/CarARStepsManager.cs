﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarARStepsManager : MonoBehaviour
{
    [System.Serializable]
    public class StepAR {
        public int StepNumber;
        public GameObject ARObject;

        public StepAR(int stepNumber, GameObject arObject){
            StepNumber = stepNumber;
            ARObject = arObject;
        }
    }
    public List<StepAR> _StepsAR = new List<StepAR>();

    CarManager _CarManager;
    // Start is called before the first frame update
    void Start()
    {
        _CarManager = GameObject.Find("Managers").GetComponent<CarManager>();
    }

    // Update is called once per frame
    void Update()
    {
        for(int i = 0; i < _StepsAR.Count; i++){
            if(_CarManager._CurrentStep == _StepsAR[i].StepNumber){
                if(!_StepsAR[i].ARObject.activeInHierarchy){
                    _StepsAR[i].ARObject.SetActive(true);
                }
            }else{
                _StepsAR[i].ARObject.SetActive(false);
            }
        }
    }
}
