﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;
using UnityEngine.UI;

public class CMSManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _CarListPrefab;
    [SerializeField]
    private Transform _CarListParent;
    [SerializeField]
    private ARManager _ARManager;
    [SerializeField]
    private CarManager _CarManager;
    [SerializeField]
    private int _CarNum;
    List<CarManager.Instruction> instruction;
    Texture2D newTexture;

    private void Start()
    {
        WebServiceWrapper.Instance.GetData("/instructions", x =>
        {
            Debug.Log("Got response! " + x, this);

            CMSData[] data = JsonConvert.DeserializeObject<CMSData[]>(x);
            Debug.Log(data[0].acf.car_make);
            Debug.Log("Number of cars: " + data.Length);
            for(int i = 0; i < data.Length; i++){
                GameObject car = Instantiate(_CarListPrefab, transform.position, Quaternion.identity, _CarListParent);
                car.transform.GetChild(0).GetComponent<Text>().text = data[i].acf.car_make + " " + data[i].acf.car_model;
                car.GetComponent<ChosenCarNumber>()._Number = data[i].id;
            }
            

            /*WebServiceWrapper.Instance.GetImageData(data[0].acf.image, texture =>
            {
                image.texture = texture;
            });*/
        });
    }
    public void ChooseCar(string number){
        WebServiceWrapper.Instance.GetData("/instructions/" + number, x =>
        {
            Debug.Log("Got response! " + x, this);

            CMSData data = JsonConvert.DeserializeObject<CMSData>(x);
            Debug.Log(data.acf.car_make);
            _CarManager._CarMake = data.acf.car_make;
            _CarManager._CarModel = data.acf.car_model;
            _CarManager._CarVIN = data.acf.car_vin;
            _CarManager._3DModelURL = data.acf.model_3d;
            _CarManager._PartsListURL = data.acf.parts_list;
            _CarManager._GearListURL = data.acf.gear_list;

            instruction = _CarManager._Instruction;
            for(int i = 0; i < data.acf.instructions.Length; i++){
            
                for(int j = 0; j < data.acf.instructions[i].instruction.Length; j++){
                    /*if(data.acf.instructions[i].instruction[j].instruction_image != "false"){
                        WebServiceWrapper.Instance.GetImageData(data.acf.instructions[i].instruction[j].instruction_image, texture =>
                        {
                            newTexture = texture;
                        });
                    }*/
                    if(data.acf.instructions[i].instruction[j].language == LanguageManager._Language){
                    instruction.Add(new CarManager.Instruction(data.acf.instructions[i].instruction[j].language,
                        data.acf.instructions[i].instruction[j].instruction_title,
                        data.acf.instructions[i].instruction[j].instruction_description,
                        data.acf.instructions[i].instruction[j].instruction_image,
                        null,
                        data.acf.instructions[i].instruction[j].instruction_video,
                        data.acf.instructions[i].instruction[j].instruction_3d,
                        data.acf.instructions[i].instruction[j].option));
                    }
                    
                }
                _CarManager.instructions.Add(new CarManager.Instructions(
                   instruction
                ));
                instruction.Clear();
            }
            
            _CarManager.LoadCar();
            

            
        });
        
        
    }
}

[Serializable]
public class CMSData
{
    public AllInstructions acf;
    public string id;
}

[Serializable]
public class AllInstructions
{
    public string car_make;
    public string car_model;
    public string car_vin;
    public string model_3d;
    public string parts_list;
    public string gear_list;
    public Instructions[] instructions;
}
[Serializable]
public class Instructions
{
    public Instruction[] instruction;
}
[Serializable]
public class Instruction
{
    public string language;
    public string instruction_title;
    public string instruction_description;
    public string instruction_image;
    public string instruction_video;
    public string instruction_3d;
    public string option;
}
