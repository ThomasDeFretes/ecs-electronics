﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageZoomer : MonoBehaviour
{
    public RectTransform _Content;
    public void ZoomIn(){
        if(_Content.localScale.x < 3f){
            float oldScale = _Content.localScale.x;
            float newScale = oldScale += 0.5f;
            _Content.localScale = new Vector3(newScale,newScale,newScale);
        }
    }
    public void ZoomOut(){
        if(_Content.localScale.x > 1f){
            float oldScale = _Content.localScale.x;
            float newScale = oldScale -= 0.5f;
            _Content.localScale = new Vector3(newScale,newScale,newScale);
        }
    }
}
