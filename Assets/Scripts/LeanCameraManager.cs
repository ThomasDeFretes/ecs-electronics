﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeanCameraManager : MonoBehaviour
{
    Transform _mainCamera;
    // Start is called before the first frame update
    void Start()
    {
        _mainCamera = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, _mainCamera.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
    }
}
