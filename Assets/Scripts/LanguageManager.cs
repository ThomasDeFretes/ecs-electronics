﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageManager : MonoBehaviour
{
    public static string _Language = "NL";
    public void ChangeLanguage(string language){
        _Language = language;
    }
}
