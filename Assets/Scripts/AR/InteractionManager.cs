﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System;
using UnityEngine.UI;

public class InteractionManager : MonoBehaviour
{
    public Camera ARCamera;
    public GameObject objectToPlace;
    public GameObject placementIndicator;
    public ARRaycastManager raycastManager;
    private ARSessionOrigin arOrigin;
    private Pose placementPose;
    private bool placementPoseIsValid = false;
    public int numberOfPointsPlaced = 0;
    public GameObject referencePointManager;
    public ARPlaneManager planeManager;
    TrackableId currentTrackableID;
    GameObject placedObject = null;
    public IndicatorColor indicatorColor;
    public ARPointCloudManager pointCloudManager;
    Vector3 indicatorStartPos;
    //private static List<ARReferencePoint> referencePoints = new List<ARReferencePoint>();

    void Start()
    {
        arOrigin = FindObjectOfType<ARSessionOrigin>();
        indicatorStartPos = placementIndicator.transform.localPosition;
    }

    void Update()
    {
#if UNITY_EDITOR
        // Used for debugging.
        if (Input.GetKeyDown(KeyCode.P))
        {
            PlaceObject();
        }
#endif

        UpdatePlacementPose();
        UpdatePlacementIndicator();

        if(placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && numberOfPointsPlaced < 1){
            PlaceObject();
            numberOfPointsPlaced++;
        }
    }
    public void ResetPlacement(){
        Destroy(placedObject);
        numberOfPointsPlaced = 0;
        placementIndicator.SetActive(false);
        pointCloudManager.enabled = true;
        this.gameObject.SetActive(false);
        
    }

    private void PlaceObject()
    {
         //ARReferencePoint referencePoint = referencePointManager.GetComponent<ARReferencePointManager>().AttachReferencePoint(planeManager.GetPlane(currentTrackableID),placementPose);

        /*if(referencePoints == null){
            Debug.Log("There was an error creating a reference point");
        }else{
            referencePoints.Add(referencePoint);
        }*/
        placedObject = Instantiate(objectToPlace, placementPose.position, placementPose.rotation);
        placementIndicator.SetActive(false);
        GameObject[] allPointCloud = GameObject.FindGameObjectsWithTag("pointCloud");
        for(int i = 0; i < allPointCloud.Length; i++){
            Destroy(allPointCloud[i]);
        }
        pointCloudManager.enabled = false;
        this.gameObject.SetActive(false);
    }
    public void ActivateIndicator(){
        placementIndicator.SetActive(true);
    }
    private void UpdatePlacementIndicator()
    {
        if(placementPoseIsValid){
            if(numberOfPointsPlaced < 1){
                indicatorColor.GreenColor();
                placementIndicator.transform.parent = null;
                placementIndicator.transform.SetPositionAndRotation( new Vector3(placementPose.position.x, placementPose.position.y + 0.01f, placementPose.position.z), placementPose.rotation);
            }else{
                //placementIndicator.SetActive(false);
                indicatorColor.WhiteColor();
                placementIndicator.transform.parent = Camera.main.transform;
                placementIndicator.transform.localPosition = indicatorStartPos;
            }
        }else{
            //placementIndicator.SetActive(false);
            indicatorColor.WhiteColor();
            placementIndicator.transform.parent = Camera.main.transform;
            placementIndicator.transform.localPosition = indicatorStartPos;
        }
    }

    private void UpdatePlacementPose()
    {
        var screenCenter = ARCamera.ViewportToScreenPoint(new Vector3(0.5f,0.5f));
        var hits = new List<ARRaycastHit>();
        raycastManager.Raycast(screenCenter, hits, TrackableType.Planes);
        placementPoseIsValid = hits.Count > 0;
        if(placementPoseIsValid){
            placementPose = hits[0].pose;
            currentTrackableID = hits[0].trackableId;

            var cameraForward = Camera.main.transform.forward;
            var cameraBearing = new Vector3(cameraForward.x,0,cameraForward.z).normalized;
            placementPose.rotation = Quaternion.LookRotation(cameraBearing);
        }
    }
}
