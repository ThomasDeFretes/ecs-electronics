﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorColor : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    void Update() {
       
    }
    private void OnEnable() {
        for(int i = 0; i < transform.childCount; i++){
            transform.GetChild(i).GetComponent<Renderer>().material.color = Color.white;
        }
    }

    public void GreenColor(){
        for(int i = 0; i < transform.childCount; i++){
            transform.GetChild(i).GetComponent<Renderer>().material.color = new Color32(0,255,29,255);
        }
    }
    public void WhiteColor(){
        for(int i = 0; i < transform.childCount; i++){
            transform.GetChild(i).GetComponent<Renderer>().material.color = Color.white;
        }
    }
}
