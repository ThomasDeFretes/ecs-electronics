﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChosenCarNumber : MonoBehaviour
{
    public string _Number;
    CMSManager _CMSManager;
    GameObject _PageChooseMenu;
    ARManager _ARManager;
    CarManager _CarManager;
    private void Start() {
        if(!_CMSManager){
            _CMSManager = GameObject.Find("Managers").GetComponent<CMSManager>();
        }
        if(!_PageChooseMenu){
            _PageChooseMenu = GameObject.Find("ChooseMenu").gameObject;
        }
        if(!_ARManager){
            _ARManager = GameObject.Find("Managers").GetComponent<ARManager>();
        }
        if(!_CarManager){
            _CarManager = GameObject.Find("Managers").GetComponent<CarManager>();
        }
    }
    public void ChooseCar(){

        _CMSManager.ChooseCar(_Number);
        _ARManager.ActivateAR();
    }
}
