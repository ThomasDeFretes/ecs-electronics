﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
//using Mapbox.Json;
using UnityEngine;
using UnityEngine.Networking;

public class WebServiceWrapper : MonoBehaviour
{

    public static WebServiceWrapper Instance
    {
        get;
        private set;
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void GetData(string path, Action<string> callback)
    {
        //Debug.Log("GET: " + path, this);

        UnityWebRequest request = UnityWebRequest.Get(GlobalSettings.API_RouteEndpoint + path);
        StartCoroutine(SendRequest(request, callback));
    }

    public void GetImageData(string path, Action<Texture2D> callback)
    {
        //Debug.Log("GET IMAGE DATA: " + path, this);

        UnityWebRequest request = UnityWebRequestTexture.GetTexture(path);
        StartCoroutine(SendImageRequest(request, callback));
    }

    public void PostData(string path, string data, Action<string> callback)
    {
        //Debug.Log("POST: " + path, this);
        //Debug.Log("Request body: " + data, this);

        // We setup request in a different way to allow posting JSON. (details: https://forum.unity.com/threads/unitywebrequest-post-url-jsondata-sending-broken-json.414708/)
        UnityWebRequest request = new UnityWebRequest(GlobalSettings.API_RouteEndpoint + path, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(data);

        if (data != "")
        {
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        }

        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        StartCoroutine(SendRequest(request, callback));
    }

    public void PostFileData(string path, List<IMultipartFormSection> form, Action<string> callback)
    {
        Debug.Log(path, this);

        // Unique boundary is used to split up multiple files. It needs to be unique so it won't match anything else in the body.
        byte[] boundary = UnityWebRequest.GenerateBoundary();
        byte[] formSections = UnityWebRequest.SerializeFormSections(form, boundary);

        // Create closing boundary to close the body.
        byte[] closingBoundary = Encoding.UTF8.GetBytes(String.Concat("\r\n--", Encoding.UTF8.GetString(boundary), "--"));

        // Merge bytes into final array to create the final body.
        byte[] body = new byte[formSections.Length + closingBoundary.Length];
        Buffer.BlockCopy(formSections, 0, body, 0, formSections.Length);
        Buffer.BlockCopy(closingBoundary, 0, body, formSections.Length, closingBoundary.Length);

        // Set up request.
        UnityWebRequest request = new UnityWebRequest(GlobalSettings.API_RouteEndpoint + path, "POST");

        UploadHandler uploadHandler = new UploadHandlerRaw(body);
        uploadHandler.contentType = String.Concat("multipart/form-data; boundary=", Encoding.UTF8.GetString(boundary));
        request.uploadHandler = uploadHandler;

        request.downloadHandler = new DownloadHandlerBuffer();

        StartCoroutine(SendRequest(request, callback));
    }

    public void PutData(string path, string data, Action<string> callback)
    {
        //Debug.Log("PUT: " + path, this);
        //Debug.Log("Request body: " + data, this);

        UnityWebRequest request = UnityWebRequest.Put(GlobalSettings.API_RouteEndpoint + path, data);
        request.SetRequestHeader("Content-Type", "application/json");
        StartCoroutine(SendRequest(request, callback));
    }

    public void DeleteData(string path, Action<string> callback)
    {
        //Debug.Log("DELETE: " + path, this);
        //Debug.Log("Request body: " + data, this);

        UnityWebRequest request = UnityWebRequest.Delete(GlobalSettings.API_RouteEndpoint + path);
        StartCoroutine(SendRequest(request, callback));
    }

    private IEnumerator SendRequest(UnityWebRequest request, Action<string> callback)
    {
        //Debug.Log("SendRequest", this);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.LogError(request.error);
            callback("");
        }
        else
        {
            //Debug.Log("Response body: " + request.downloadHandler.text, this);

            if (request.downloadHandler != null)
            {
                callback(request.downloadHandler.text);
            }
            else
            {
                callback("Request completed, got response!");
            }
        }
    }

    private IEnumerator SendImageRequest(UnityWebRequest request, Action<Texture2D> callback)
    {
        Debug.Log("SendImageRequest " + request.url);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            Texture2D texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            callback(texture);
        }
    }
}