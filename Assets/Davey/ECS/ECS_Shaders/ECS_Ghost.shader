// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ECS_Ghost"
{
	Properties
	{
		_Emission_Foreground("Emission_Foreground", Color) = (1,0.9882353,0.1058824,0)
		_Emission_Background("Emission_Background", Color) = (1,0.7318864,0.1058823,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent-2100" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha noshadow 
		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
		};

		uniform float4 _Emission_Foreground;
		uniform float4 _Emission_Background;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = i.worldNormal;
			float fresnelNdotV5 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode5 = ( 0.0 + 2.5 * pow( 1.0 - fresnelNdotV5, 2.5 ) );
			float4 lerpResult10 = lerp( _Emission_Foreground , _Emission_Background , saturate( fresnelNode5 ));
			o.Emission = lerpResult10.rgb;
			float mulTime54 = _Time.y * -0.025;
			o.Alpha = (0.33 + (frac( ( ( ( ( 1.0 - ase_worldPos.y ) + mulTime54 ) + 0.0 ) * 2.0 ) ) - 0.0) * (0.66 - 0.33) / (1.0 - 0.0));
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16400
2079;2;1906;1004;2771.226;441.688;2.054991;True;True
Node;AmplifyShaderEditor.WorldPosInputsNode;45;-2270.292,425.5329;Float;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.OneMinusNode;64;-2029.545,475.3534;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;54;-2018.588,657.8663;Float;False;1;0;FLOAT;-0.025;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;52;-1795.804,498.2291;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;49;-1546.386,698.7767;Float;False;Constant;_Float2;Float 2;-1;0;Create;True;0;0;False;0;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;48;-1588.41,455.2547;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FresnelNode;5;-1144.927,-245.9017;Float;False;Standard;WorldNormal;ViewDir;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;2.5;False;3;FLOAT;2.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;50;-1311.38,474.3354;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;11;-1033.281,285.3419;Float;False;Property;_Emission_Background;Emission_Background;1;0;Create;True;0;0;False;0;1,0.7318864,0.1058823,0;1,0.7318864,0.1058823,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;58;-786.2915,50.15051;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;51;-1074.359,481.4707;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;2;-1034.034,112.39;Float;False;Property;_Emission_Foreground;Emission_Foreground;0;0;Create;True;0;0;False;0;1,0.9882353,0.1058824,0;1,0.9882353,0.1058824,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;10;-608.5222,183.2516;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TFHCRemapNode;62;-850.5454,472.3534;Float;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0.33;False;4;FLOAT;0.66;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;13;-662.6139,-136.9293;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-1106.647,-76.09195;Float;False;Property;_Opacity;Opacity;2;0;Create;True;0;0;False;0;0.5;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;15;-501.9969,-133.7383;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;14;-829.9974,-139.7382;Float;False;2;0;FLOAT;0;False;1;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;116.1678,-62.34234;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;ECS_Ghost;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;-2100;False;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;64;0;45;2
WireConnection;52;0;64;0
WireConnection;52;1;54;0
WireConnection;48;0;52;0
WireConnection;50;0;48;0
WireConnection;50;1;49;0
WireConnection;58;0;5;0
WireConnection;51;0;50;0
WireConnection;10;0;2;0
WireConnection;10;1;11;0
WireConnection;10;2;58;0
WireConnection;62;0;51;0
WireConnection;13;0;14;0
WireConnection;13;1;3;0
WireConnection;13;2;5;0
WireConnection;15;0;13;0
WireConnection;14;0;3;0
WireConnection;0;2;10;0
WireConnection;0;9;62;0
ASEEND*/
//CHKSM=7A760573B56D6845E06841204B76495A19D5A6A4