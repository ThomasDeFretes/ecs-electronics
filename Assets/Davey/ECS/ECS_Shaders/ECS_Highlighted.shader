// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ECS_Highlighted"
{
	Properties
	{
		_Emission_Foreground("Emission_Foreground", Color) = (1,0,0,0)
		_Emission_Background("Emission_Background", Color) = (0,0,0,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent-2000" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha noshadow 
		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
		};

		uniform float4 _Emission_Foreground;
		uniform float4 _Emission_Background;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = i.worldNormal;
			float fresnelNdotV5 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode5 = ( 0.0 + 2.5 * pow( 1.0 - fresnelNdotV5, 2.5 ) );
			float4 lerpResult10 = lerp( _Emission_Foreground , _Emission_Background , saturate( fresnelNode5 ));
			o.Emission = lerpResult10.rgb;
			float mulTime70 = _Time.y * 4.0;
			o.Alpha = (0.25 + (sin( mulTime70 ) - -1.0) * (0.75 - 0.25) / (1.0 - -1.0));
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16400
2079;2;1906;1004;1349.22;226.5091;1.010296;True;True
Node;AmplifyShaderEditor.FresnelNode;5;-1144.927,-245.9017;Float;False;Standard;WorldNormal;ViewDir;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;2.5;False;3;FLOAT;2.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;70;-564.2194,385.7299;Float;False;1;0;FLOAT;4;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;58;-786.2915,50.15051;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;71;-351.047,358.4521;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;2;-1034.034,112.39;Float;False;Property;_Emission_Foreground;Emission_Foreground;0;0;Create;True;0;0;False;0;1,0,0,0;1,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;11;-1033.281,285.3419;Float;False;Property;_Emission_Background;Emission_Background;1;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;10;-608.5222,183.2516;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;13;-662.6139,-136.9293;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-1106.647,-76.09195;Float;False;Property;_Opacity;Opacity;2;0;Create;True;0;0;False;0;0.5;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;15;-501.9969,-133.7383;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;14;-829.9974,-139.7382;Float;False;2;0;FLOAT;0;False;1;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;62;-189.8118,251.0988;Float;False;5;0;FLOAT;0;False;1;FLOAT;-1;False;2;FLOAT;1;False;3;FLOAT;0.25;False;4;FLOAT;0.75;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;114.1128,-58.23236;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;ECS_Highlighted;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;-2000;False;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;58;0;5;0
WireConnection;71;0;70;0
WireConnection;10;0;2;0
WireConnection;10;1;11;0
WireConnection;10;2;58;0
WireConnection;13;0;14;0
WireConnection;13;1;3;0
WireConnection;13;2;5;0
WireConnection;15;0;13;0
WireConnection;14;0;3;0
WireConnection;62;0;71;0
WireConnection;0;2;10;0
WireConnection;0;9;62;0
ASEEND*/
//CHKSM=336099A30733E56F8129E85785975FB47B1682A6