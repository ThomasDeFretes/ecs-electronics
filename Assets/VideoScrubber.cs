﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoScrubber : MonoBehaviour
{
    [SerializeField]
    private RectTransform _VideoBar;
    [SerializeField]
    private VideoPlayer _VideoPlayer;
    float _Progression;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        _Progression = (_VideoBar.transform.parent.GetComponent<RectTransform>().sizeDelta.x * ((float)_VideoPlayer.time/(float)_VideoPlayer.length));
        _VideoBar.sizeDelta = new Vector2(_Progression, 2f);
    }
}
