﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _PlacementIndicator;
    [SerializeField]
    private GameObject _InteractionManager;
    [SerializeField]
    private GameObject _ChoosePage;
    [SerializeField]
    private GameObject _InstructionsPage;
    

    public void ActivateAR(){
        _PlacementIndicator.SetActive(true);
        _InteractionManager.SetActive(true);
        _InstructionsPage.SetActive(true);
        _ChoosePage.SetActive(false);
    }
}
