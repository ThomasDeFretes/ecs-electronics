﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class InstructionScript : MonoBehaviour
{
    public Text _Title;
    public Text _Description;
    public Image _InstructionImage;
    public Image _InstructionImageLarge;
    public VideoPlayer _VideoPlayer;
    public RenderTexture _RenderTexture;
    public RawImage _RawImage;
    public GameObject _PauseBttn;
    bool _Started = false;
    void Start(){
        _RenderTexture = new RenderTexture(1280, 720, 16, RenderTextureFormat.ARGB32);
        _RenderTexture.Create();
        _VideoPlayer.targetTexture = _RenderTexture;
        _RawImage.texture = _RenderTexture;
        
    }
    public void ToggleVideo(){
        if(!_VideoPlayer.isPlaying){
            _VideoPlayer.Play();
            _Started = true;
        }else{
            _VideoPlayer.Pause();
        }
    }
    void Update(){
        if(!_Started && _VideoPlayer.isPlaying){
            _VideoPlayer.Pause();
        }
        if(_VideoPlayer.isPlaying){
            _PauseBttn.SetActive(false);
        }else{
            _PauseBttn.SetActive(true);
        }
    }
    
}
