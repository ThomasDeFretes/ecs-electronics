﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingBar : MonoBehaviour
{
    [SerializeField]
    private RectTransform _LoadingBar;
    public float _Progression;
    [SerializeField]
    private Text _LoadingText;
    [SerializeField]
    private List<string> _ProgressionText = new List<string>();
    private float _TextStepLenght;
    [SerializeField]
    private GameObject _ObjectToDisable;

    void Start(){
        _TextStepLenght = 100 / (_ProgressionText.Count - 1);
    }
    
    void Update()
    {
        _LoadingText.text = _ProgressionText[Mathf.FloorToInt(_Progression / _TextStepLenght)];
        _LoadingBar.sizeDelta = new Vector2(_Progression, 10f);
        _Progression+= 0.1f;
        if(_Progression >= 100){
            _ObjectToDisable.SetActive(false);
        }
    }
}
